# Beispielstruktur
max_num = int(input("Bitte geben Sie die maximale Zahl ein: "))
# Primzahlen finden

def is_prime(num):
    if num <= 1:
        return False
    if num <= 3:
        return True
    if num % 2 == 0 or num % 3 == 0:
        return False
    i = 5
    while i * i <= num:
        if num % i == 0 or num % (i + 2) == 0:
            return False
        i += 6
    return True


print("Primzahlen bis", max_num, "sind:")
for number in range(2, max_num + 1):
